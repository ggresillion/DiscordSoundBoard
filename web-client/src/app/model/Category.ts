import Song from './Song';

export default class Category {
  public name: string;
  public songs: Song[];
}
